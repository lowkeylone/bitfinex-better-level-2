let btl__observer;

function btl__init() {
    $(".split__container .split__alt .footer__wrapper").prepend("<button class='ui-button ui-button--size-XS ui-button--clear' id='runImprovedTradesList'><i class='fa fa-usd'></i></button>");
    $("#runImprovedTradesList").click(function() {
        btl__start();
    });
}

function btl__start() {
    console.log('[BBL2] Bitfinex Better Trades List started');

    $("#trading-widget-1 .trades tbody > tr").each(function() {
        btl__setUSDAmountTooltip($(this));
    });

    btl__observer = new MutationObserver(btl__onMutation);
    btl__observer.observe($("#trading-widget-1 .trades tbody").first()[0], { childList: true });

    $("#runImprovedTradesList").addClass("ui-button--disabled").off('click');
}

function btl__setUSDAmountTooltip($parentNode) {
    try {
        const $amountNode = $parentNode.find("td.col-currency > span").first();
        $amountNode.addClass("bbl2-tooltip");
        const $priceNode = $parentNode.find("td").not(".col-currency").last();

        const amount = parseFloat($amountNode.text().replace(/,/g, ""));
        const price = parseFloat($priceNode.text().replace(/,/g, ""));

        const usdAmount = Math.round(amount * price);

        $amountNode.append("<span class='tooltip-text'>$"+ formatNumber(usdAmount) +"</span>");
    }
    catch(error) {
        console.error(error);
    }
}

function btl__onMutation(mutations) {
    mutations.forEach(function( mutation ) {
        const newNodes = mutation.addedNodes;

        if (newNodes !== null) {
            const $newNodes = $(newNodes);
            $newNodes.each(function() {
                btl__setUSDAmountTooltip($(this));
            });
        }
    });
}